from rest_framework import serializers, validators

from reunidos_backend.api.models import (
    Organization, Campaign, Donation, RequestedItem)


class OrganizationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Organization


class CampaignSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Campaign


class DonationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Donation


class RequestedItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RequestedItem
