from rest_framework import viewsets
from reunidos_backend.api.serializers import (
    OrganizationSerializer, CampaignSerializer,
    DonationSerializer, RequestedItemSerializer)
from reunidos_backend.api.models import (
    Organization, Campaign, Donation, RequestedItem)


class OrganizationViewSet(viewsets.ModelViewSet):
    serializer_class = OrganizationSerializer
    queryset = Organization.objects.all()


class CampaignViewSet(viewsets.ModelViewSet):
    serializer_class = CampaignSerializer
    queryset = Campaign.objects.all()


class DonationViewSet(viewsets.ModelViewSet):
    serializer_class = DonationSerializer
    queryset = Donation.objects.all()


class RequestedItemViewSet(viewsets.ModelViewSet):
    serializer_class = RequestedItemSerializer
    queryset = RequestedItem.objects.all()
