from django.contrib import admin

from reunidos_backend.api.models import (
    Organization, Campaign, Donation, Shipment,
    RequestedItem, DonatedItem, ShippedItem)

admin.site.register(Organization)
admin.site.register(Campaign)
admin.site.register(Donation)
admin.site.register(Shipment)

admin.site.register(RequestedItem)
admin.site.register(DonatedItem)
admin.site.register(ShippedItem)
