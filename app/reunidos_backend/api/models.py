# -*- coding: UTF-8 -*-
import os
from glob import iglob
from django.conf import settings
from django.db import models
from django.utils.text import slugify


def upload_images(instance, filename):
    #FIXME slug collisions
    basepath = '{}organizations/{}'.format(
        settings.MEDIA_ROOT, slugify(instance.name))
    os.makedirs(basepath, mode=0o755, exist_ok=True)

    for f in iglob('{}/*'.format(basepath)):
        os.remove(f)

    return '{}/logo'.format(basepath)


class Organization(models.Model):
    """ Organization that creates and manages campaigns (ONGs, etc.) """
    name = models.CharField(unique=True, max_length=120)
    logo = models.ImageField(upload_to=upload_images)
    description = models.TextField()
    active = models.BooleanField(default=False)
    date_registered = models.DateTimeField(auto_now_add=True)
    date_activated = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '<{} {}>'.format(self.__class__.__name__, self.name)


class Campaign(models.Model):
    """ An active campaign """
    organization = models.ForeignKey(Organization)
    name = models.CharField(max_length=120)
    description = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    expires = models.DateTimeField()
    auto_renew = models.BooleanField(default=False)

    def __str__(self):
        return '<{} {} ({})>'.format(self.__class__.__name__,
            self.name, self.organization.name)


class Donation(models.Model):
    """ Received money """
    campaign = models.ForeignKey(Campaign)
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    donor_name = models.CharField(max_length=120)
    donor_email = models.EmailField()
    receive_feedback = models.BooleanField(default=True)
    date_received = models.DateTimeField(auto_now_add=True)


class Shipment(models.Model):
    """ Delivered item or items by a provider """
    provider = models.CharField(max_length=120)
    date_delivered = models.DateTimeField()


class Item(models.Model):
    UNITS = (
        ('kg', 'Kilogramos'),
        ('lt', 'Litros'),
        ('m', 'Metros'),
        ('m2', 'Metros cuadrados'),
        ('m3', 'Metros cúbicos'),
        ('unit', 'Unidades'),
    )

    name = models.CharField(max_length=120)
    #image = models.ImageField(upload_to=upload_item_imgs)
    amount = models.IntegerField()
    unit = models.CharField(max_length=12, choices=UNITS)

    class Meta:
        abstract = True

    def __str__(self):
        return '{} ({} {})'.format(self.name, self.amount, self.unit)


class RequestedItem(models.Model):
    campaign = models.ForeignKey(Campaign)
    estimated_cost = models.DecimalField(max_digits=6, decimal_places=2)


class DonatedItem(models.Model):
    campaign = models.ForeignKey(Campaign)
    cost = models.DecimalField(max_digits=6, decimal_places=2)


class ShippedItem(models.Model):
    campaign = models.ForeignKey(Campaign)
