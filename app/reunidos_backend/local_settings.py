# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'a-long-and-random-string-for-cryptographic-purposes'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

MEDIA_ROOT = '/tmp/'
