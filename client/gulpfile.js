var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var cleanCss = require('gulp-clean-css');
var webserver = require('gulp-webserver');
var sequence = require('run-sequence');
var plugins = require('gulp-load-plugins')();
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');

/*
	Another libraries:
	- Gulp linters
	- Tests (MochaJs, KarmaJs)
	- Uglify
*/

var CSS_DEPS = [
  'node_modules/angular-material/angular-material.min.css',
]

var JS_DEPENDENCIES = [
  'node_modules/angular/angular.js',
  'node_modules/angular-ui-router/release/angular-ui-router.js',
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/angular-resource/angular-resource.min.js',
  'node_modules/angular-aria/angular-aria.min.js',
  'node_modules/angular-animate/angular-animate.min.js',
  'node_modules/angular-material/angular-material.min.js',
  'node_modules/angular-messages/angular-messages.min.js'
];

var PATHS = {
  sass: ['/app/src/scss/*.scss', './app/src/scss/**/*.scss'],
  js: ['app/src/js/*.js', 'app/src/js/**/*.js', '!app/src/js/vendors/*.js'],
  html: ['./app/src/js/**/templates/*.html', './app/src/js/**/templates/**/*.html'],
  img: ['app/src/img/*.png', 'app/src/img/*.jpg', 'app/src/img/*.jpeg']
};

gulp.task('html', function (done) {
	gulp.src(PATHS.html)
    .on('end', done);
});
gulp.task('sass', function (done) {
  var task = gulp.src(PATHS.sass);
  task = task
    .pipe(sass())
    .pipe(rename({ extname: '.css' }))
    .pipe(cleanCss());
  task
    .pipe(gulp.dest('./app/dist/css/'))
    .on('end', done);
});
gulp.task('ext-css', function (done) {
  var task = gulp.src(CSS_DEPS);
  task = task
    .pipe(rename({ extname: '.css' }))
    .pipe(cleanCss());
  task
    .pipe(gulp.dest('./app/dist/css/'))
    .on('end', done);
});
gulp.task('js', function (done) {
  var task;
  task = gulp
  	.src(PATHS.js)
    .pipe(plugins.concat('app.min.js'))
    .pipe(plugins.babel({
      presets: ['es2015-loose'],
      plugins: [
        'syntax-class-properties',
        'transform-class-properties'
      ]
    }))
    .pipe(uglify());
  task
    .pipe(gulp.dest('./app/dist/js'))
    .on('end', function () {
        gulp
          .src(JS_DEPENDENCIES)
          .pipe(plugins.concat('libs.min.js'))
          .pipe(uglify())
          .pipe(gulp.dest('./app/dist/js'))
          .on('end', function () {
            gulp
              .src('./app/src/js/vendors/**.js')
              .pipe(plugins.concat('vendors.min.js'))
              .pipe(uglify())
              .pipe(gulp.dest('./app/dist/js'))
              .on('end', done);
          });
    });
});
gulp.task('server', function () {
	gulp.src('./app').pipe(webserver({
		livereload: false,
	  host: 'localhost',
    port: 6500
	}));
});
gulp.task('minify-image', function (done) {
  gulp.src(PATHS.img)
    .pipe(imagemin())
    .pipe(gulp.dest('./app/dist/img'))
    .on('end', done);
});
gulp.task('build', function (cb) {
  sequence(['sass', 'html', 'js'], cb);
});
gulp.task('watch', ['js', 'sass', 'html', 'server'], function () {
  gulp.watch(PATHS.js, { interval: 1000 }, ['js']);
  gulp.watch(PATHS.sass, { interval: 1000 }, ['sass']);
  gulp.watch(PATHS.html, { interval: 1000 }, ['html']);
});
gulp.task('default', ['watch']);
