(function (ng) {
	'use strict';

 	ng.module('app.campaigns', ['ui.router', 'app.api', 'app.donations', 'ngMaterial', 'ngMaterial'])
		.config(['$stateProvider', function ($stateProvider) {
			$stateProvider
				.state('app.campaigns', {
					'url': '/campaigns',
					'abstract': true,
					'views': {
						'main': {
							'templateUrl': 'src/js/campaigns/templates/views/base-campaigns.html'
						}
					}
				})
				.state('app.campaigns.list', {
					'url': '/list',
					'views': {
						'campaign': {
							'templateUrl': 'src/js/campaigns/templates/views/campaign-list.html',
							'controller': 'CampaignListController as CampaignList'
						}
					}
				})
				.state('app.campaigns.detail', {
					'url': '/detail/:id',
					'views': {
						'campaign': {
							'templateUrl': 'src/js/campaigns/templates/views/campaign-detail.html',
							'controller': 'CampaignDetailController as CampaignDetail',
							'resolve': {
								'id': ['$stateParams', function ($stateParams) {
									return $stateParams.id;
								}]
							}
						}
					}
				});

				// appAPI.registerEndpoints([{
        //   name: 'Campaigns',
        //   rest: {
        //     url: '/campaigns/:id',
        //     defaultParams: { id: '@id' }
        //   }
        // }]);
			}
		])
		.run(['$rootScope', '$state', function ($rootScope, $state) {
		}]);
})(angular);
