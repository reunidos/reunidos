// ((ng) => {
//   'use strict';
//
//   ng.module('app.campaigns')
//     .provider('apiCampaign', function () {
//       var Campaigns = function ($q, api) {
//         // var $instance = this;
//
//         this.getAllCampaigns = function () {
//           return api.Campaigns.query().$promise;
//         };
//
//         this.getCampaignById = function (id) {
//           return api.Campaigns.get(id).$promise;
//         };
//       };
//
//       this.$get = ['$q', 'api', function ($q, api) {
//         return new Campaigns($q, api);
//       }];
//     });
// })(angular);
