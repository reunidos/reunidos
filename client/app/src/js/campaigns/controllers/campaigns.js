((ng) => {
  'use strict';

  var CampaignDetailController = function ($scope, $http, $state, DonationService, id, $mdToast) {
    initProperties();
    loadProperties();

    function initProperties () {
      $scope.campaign = {};
      $scope.items = [];
      $scope.bonus = [];

      $scope.singleDonation = {};

      $scope.total = 0;
    }
    function loadProperties () {
      $http.get('http://127.0.0.1:8000/api/v1/campaign/' + id).then(function (response) {
          $scope.campaign = response.data;
        },
        function (err) { console.error(err); }
      );
      $http.get('http://127.0.0.1:8000/api/v1/item/?campaign=' + id)
        .then(function (response) {
          $scope.items = response.data;
          $scope.items.forEach(function (item) {
            $http.get('http://127.0.0.1:8000/api/v1/bono/?item=' + item.id)
              .then(function (response) {
                response.data.forEach((bonusRow) => {
                  $scope.bonus.push(bonusRow);
                });
              });
          });
        });
    }

    $scope.confirmDonation = function () {
      if (Object.keys($scope.singleDonation).length > 0) {
        DonationService.loadDataDonation('singleDonation', $scope.singleDonation);
        $state.go('app.donations.confirm');
      } else {
        $mdToast.show(
          $mdToast.simple()
          .textContent('Debe realizar una donación')
          .position('top right')
          .hideDelay(3000));
      }
    }

    $scope.$watch('items', function (newItems, oldItems) {
      var total = 0;
      newItems.forEach(function (item, index, array) {
        total += (item.cost * item.donated);

        if (oldItems.length > 0 &&
            oldItems[index].id == newItems[index].id &&
            oldItems[index].donated !== newItems[index].donated) {
              if (!$scope.singleDonation[item.id]) {
                $scope.singleDonation[item.id] = {
                  'name': item.name,
                  'price': item.cost,
                  'initialDonation': oldItems[index].donated
                };
              }
              var iniDonation = $scope.singleDonation[item.id].initialDonation;
              $scope.singleDonation[item.id]['currentDonation'] = newItems[index].donated - iniDonation;
        }
      });

      $scope.total = total;
    }, true);
  };

  var CampaignListController = function ($scope, $http, $state) {
    function init () {
      $http.get('http://127.0.0.1:8000/api/v1/campaign').then(function (response) {
        $scope.campaigns = response.data;
      });
    }

    $scope.campaigns = [];
    init();

    $scope.viewDetailCampaign = function (campaign) {
      $state.go('app.campaigns.detail', { 'id': campaign.id });
    };
  };

  ng.module('app.campaigns')
    .controller('CampaignDetailController', ['$scope', '$http', '$state', 'DonationService', 'id', '$mdToast',
                                              CampaignDetailController])
    .controller('CampaignListController', ['$scope', '$http', '$state', CampaignListController]);
})(angular);
