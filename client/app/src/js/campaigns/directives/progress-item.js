(function (ng) {
  'use strict';

  var ProgressController = function ($scope, $timeout) {
    var bonusIndex = 0;
    var sortedBonus = [];
    var nearestBonus = {};
    function getRatioDonated () {
      return ($scope.item.donated * 100) / $scope.item.requested;
    }

    $scope.isChanged = false;
    $scope.ratioDonated = getRatioDonated();

    $scope.$watch('item.donated', function () {
      $scope.ratioDonated = getRatioDonated();

      if (nearestBonus.item &&
          $scope.item.donated == nearestBonus.monto_min &&
          $scope.item.id === nearestBonus.item.id) {

        // Effects
        var diffToAdd = nearestBonus.monto_adicionar;
        for (var i = 0; i < diffToAdd; i++) {
          $timeout(function () {
            $scope.item.donated += 1;
          }, 100);
        }
        nearestBonus = sortedBonus[++bonusIndex];

        $scope.isChanged = true;
        $timeout(function () {
          $scope.isChanged = false;
        }, 1000);
      }
    });

    $scope.$watchCollection('bonus', function () {
      if ($scope.bonus.length > 0) {
        sortedBonus = $scope.bonus.filter(function (bonus) {
          return bonus.item.id == $scope.item.id;
        }).sort(function (bonusA, bonusB) {
          return bonusA.monto_min - bonusB.monto_min;
        });
        nearestBonus = sortedBonus[bonusIndex];
      }
    });
  }

  var ProgressItem = {
    'templateUrl': 'src/js/campaigns/templates/directives/progress-item.html',
    'restrict': 'E',
    'scope': {
      'item': '=',
      'bonus': '='
    },
    'controller': ['$scope', '$timeout', ProgressController]
  };

  ng.module('app.campaigns')
    .directive('progressItem',function () {
      return ProgressItem;
    });
})(angular);
