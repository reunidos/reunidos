(function (ng) {
  'use strict';

  var headerApp = {
    'templateUrl': 'src/js/core/templates/directives/header.html',
    'restrict': 'E'
  };

  ng.module('app')
    .directive('headerApp', function () {
      return headerApp;
    });
})(angular);
