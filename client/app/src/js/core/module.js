((window, $, ng) => {
	'use strict';

	ng.module('app', ['ui.router', 'app.campaigns', 'app.donations'])
		.config(['$stateProvider', '$urlRouterProvider',
			function ($stateProvider, $urlRouterProvider) {
				$stateProvider
					.state('public', {
						'url': '/',
						'templateUrl': 'src/js/core/templates/views/public.html'
					})
					.state('app', {
						'url': '/app',
						'templateUrl': 'src/js/core/templates/views/app.html',
						'abstract': true
					});
				$urlRouterProvider.otherwise('/app/campaigns');
			}
		])
		.run(['$rootScope', '$timeout', function ($rootScope, $timeout) {
			$rootScope.$on('$stateChangeSuccess',
				function(event, toState, toParams, fromState, fromParams) {
					if (toState.name === "public" && fromState.name.length) {
						$timeout(function () {
							var $headerVideoHero = $('#header-video-hero');
							var $wallOpacity = $('.wall-opacity');
							var $window = $(window);

							window.$setSizeVideo($window, $headerVideoHero, $wallOpacity);
							window.$playIntro();
						}, 150);
					}
				}
			);
		}]);
})(window, jQuery, angular);
