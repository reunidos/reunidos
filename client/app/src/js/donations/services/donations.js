(function (ng) {
  'use strict';

  var DonationService = function () {
    this.donation = {};

    this.loadDataDonation = function (name, data) {
      this.donation[name] = data;
    }

    this.cleanDataDonation = function () {
      this.donation = {};
    }
  };

  ng.module('app.donations')
    .service('DonationService', [DonationService]);
})(angular);
