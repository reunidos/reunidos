(function (ng) {
  'use strict';

  var ConfirmDonationCtrl = function ($scope, $state, DonationService) {
    $scope.donation = DonationService.donation.singleDonation;

    $scope.confirmDetailDonation = function () {
      $state.go('app.donations.detail');
    };
  };

  var DetailDonationCtrl = function ($scope, $state, $mdToast, $timeout, DonationService) {
    $scope.showInfo = false;
    $scope.detailDonation = {
      name: '',
      city: '',
      idDocument: '',
      email: '',
      creditCard: {
        numberCard: null,
        due: null,
        securityCode: null
      }
    };

    $scope.donate = function () {
      if (Object.keys(!$scope.detailDonationForm.$error).length === 0) {
        // FIXME: Replace $timeout for real request.
        $mdToast.show(
          $mdToast.simple()
          .textContent('Gracias por realizar la donación')
          .position('top right')
          .hideDelay(3000));

        $timeout(function () {
          $state.go('public');
        }, 2000);
      } else {
        $scope.showInfo = true;
        $mdToast.show(
          $mdToast.simple()
          .textContent('Debe ingresar los datos correctos para realizar una donación')
          .position('top right')
          .hideDelay(3000));
      }
    };
  };

  ng.module('app.donations')
    .controller('ConfirmDonationController', [
      '$scope', '$state', 'DonationService', ConfirmDonationCtrl])
    .controller('DetailDonationController', [
      '$scope', '$state', '$mdToast', '$timeout', 'DonationService', DetailDonationCtrl]);
})(angular);
