(function (ng) {
  'use strict';

  ng.module('app.donations', ['ui.router', 'ngMaterial', 'ngMessages'])
    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('app.donations', {
          'url': '/donations',
          'abstract': true,
          'views': {
            'main': {
              'templateUrl': 'src/js/donations/templates/views/base-donation.html'
            }
          }
        })
        .state('app.donations.confirm', {
          'url': '/confirm',
          'views': {
            'donation': {
              'templateUrl': 'src/js/donations/templates/views/confirm-donation.html',
              'controller': 'ConfirmDonationController as ConfirmDonation'
            }
          }
        })
        .state('app.donations.detail', {
          'url': '/detail',
          'views': {
            'donation': {
              'templateUrl': 'src/js/donations/templates/views/detail-donation.html',
              'controller': 'DetailDonationController as DetailDonation'
            }
          }
        });
    }])
    .run(['$rootScope', '$stateParams', function ($rootScope, $stateParams) {

    }]);
})(angular);
