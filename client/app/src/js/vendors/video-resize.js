(function (window, $, ng) {
	'use strict';

	var $headerVideoHero, $wallOpacity, $window;

	$(document).ready(function () {
		setValues();
		initEventListeners();
		window.$setSizeVideo = $setSizeVideo;
	});

	function setValues () {
		$headerVideoHero = $('#header-video-hero');
		$wallOpacity = $('.wall-opacity');
		$window = $(window);
	}

	function initEventListeners () {
		$initialLoad();
		$resizeOnVideo();
	}

	var $initialLoad = function () {
		// FIXME: Use video load event instead of timeout.
		window.setTimeout(function () {
			$setSizeVideo();
		}, 250);
	}

	var $resizeOnVideo = function() {
		$window.resize(function () {
			$setSizeVideo();
		});
	};

	var $setSizeVideo = function ($_window, $_headerVideoHero, $_wallOpacity) {
		if ($_window && $_headerVideoHero && $_wallOpacity) {
			$window = $_window;
			$headerVideoHero = $_headerVideoHero;
			$wallOpacity = $_wallOpacity;
		}

		var sizeViewportWidth = $window.width();
		$headerVideoHero.width(sizeViewportWidth);

		var videoHeroHeight = $headerVideoHero.height();
		$wallOpacity.height(videoHeroHeight + 2);
	};
})(window, jQuery, angular);
