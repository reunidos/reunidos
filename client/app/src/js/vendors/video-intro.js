(function (window, $) {
	'use strict';

	$(document).ready(function () {
		initEventListeners();
	});

	function initEventListeners () {
		$playIntro();
		window.$playIntro = $playIntro;
	}

	var $playIntro = function() {
    var $btnVideoIntro = $('#play-intro-video');
		var $videoWall = $('.video-intro-frame');
    var $videoIntro = $('.video-intro');
    var $wallContainer = $('.wall-container');
		var _videoDom = $videoIntro.get(0);

		$btnVideoIntro.click(function () {
      $videoIntro.fadeIn('slow');
			$videoWall.fadeIn('slow');

			_videoDom.currentTime = 0;
      _videoDom.play();
    });

    $videoWall.click(function () {
      if($videoIntro.css('display') !== 'none') {
				_videoDom.pause();

        $videoIntro.fadeOut('slow');
				$videoWall.fadeOut('slow');
			}
    });
	};
})(window, jQuery);
