(function (ng) {
  'use strict';

  ng.module('app.api')
    .provider('appAPI', function () {
      var $provider = this;

      this.timeout = 15 * 1000;
      this.init = function (timeout) {
        $provider.timeout = timeout;
      };

      this.endpoints = [];
      this.registerEndpoints = function (apiEndpoints) {
        /* endpoint form: {
          name: 'countries',
          rest: {
            url: '/countries/:id'
          }
          http: {
            method: 'POST',
            url: '/countries/:id'
            data: { isoCode: '' },
            filters: { id: null },
            cacheDuration: 0
          }
        }

        */
        ng.forEach(apiEndpoints, function (endpoint) {
          if (endpoint.http) {
            endpoint.http.method = endpoint.http.method || 'GET';
            endpoint.http.data = endpoint.http.data || {};
            endpoint.http.filters = endpoint.http.filters || {};
            endpoint.http.cacheDuration = endpoint.http.cacheDuration || 1440;  // 24 hours
          }
          $provider.endpoints.push(endpoint);
        });
      };

      function API ($q, $http, $resource, API_URL) {
        var actions = {
          get: { method: 'GET' },
          save: { method: 'POST', isArray: false },
          query: { method: 'GET', isArray: true },
          remove: { method: 'DELETE' },
          delete: { method: 'DELETE' },
          update: { method: 'PUT' },
          options: { method: 'OPTIONS' }
        };

        var getCacheKey = function (name, data) {
          var urlParams = [];
          for (var key in data) {
            urlParams.push(key + '=' + data[key]);
          }

          // var user = ptSession.data.isAuthenticated ? ptSession.data.profile.email : 'anonymous';
          // return 'app:api:' + name.toLowerCase() + ':' + urlParams.join(':') + ':' + user;

          return 'app:api:' + name.toLowerCase() + ':' + urlParams.join(':');
        };

        // Set endpoints as object functions
        ng.forEach($provider.endpoints, function (endpoint) {
          if (endpoint.rest) {
            // Rest APIs use $resource
            Object.defineProperty(API.prototype, endpoint.name, {
              get: function () {
                return $resource(
                  API_URL + endpoint.rest.url,
                  endpoint.rest.defaultParams,
                  actions);
              }
            });
          } else {
            // Non Rest APIs just do a regular $http call
            API.prototype[endpoint.name] = function (data, options) {
              var deferred = $q.defer();
              data = data || [];
              options = options || { headers: {} };

              var url = endpoint.http.url;

              function doCall (httpOptions) {
                var minutes = endpoint.http.cacheDuration;
                var filters = ng.copy(endpoint.http.filters);
                var prms = {};

                for (var key in data) {
                  if (key in filters) {
                    filters[key] = data[key];
                  } else {
                    if (key in endpoint.http.data) {
                      prms[key] = data[key];
                    }
                  }
                }

                for (key in filters) {
                  url = url.replace(':' + key, filters[key]);
                }
                // Process get parameters
                if (endpoint.http.method === 'GET') {
                  var urlParams = [];
                  for (key in prms) {
                    urlParams.push(key + '=' + prms[key]);
                  }
                  if (urlParams.length) {
                    url = url + '?' + urlParams.join('&');
                  }
                }

                var cacheKey = getCacheKey(endpoint.name, data);
                if (options.cache) {
                  var currentDatetime = new Date();
                  var cacheItem = store.get(cacheKey);
                  if (cacheItem) {
                    var expires = new Date(Date.parse(cacheItem.expires));
                    if (expires > currentDatetime) {
                      deferred.resolve({ data: cacheItem.data });
                      return deferred.promise;
                    }
                  }
                }

                var reqSuccess = function (response) {
                  if (response.status >= 200 && response.status < 300) {
                    var result = response.data;
                    if (options.cache) {
                      var expires = new Date(new Date().getTime() + minutes * 60000);
                      store.set(cacheKey, {
                        data: result,
                        accessed: new Date(),
                        expires: expires
                      });
                    }
                    deferred.resolve(response);
                  } else {
                    ptException.rejector('API: Server error', deferred)(response);
                  }
                };

                var reqErr = function (err) {
                  // Timeout handling with automatic retries
                  if (err.status === 0) {
                    if (httpOptions.retry < 2) {
                      httpOptions.retry += 1;
                      doCall(httpOptions);
                    } else {
                      // ptException.noInternetRejector(deferred);
                    }
                  } else {
                    // ptException.rejector('API', deferred)(err);
                  }
                };
                if (endpoint.http.method === 'GET') {
                  $http
                    .get(API_URL + url, httpOptions)
                    .then(reqSuccess, reqErr);
                } else if (endpoint.http.method === 'POST') {   // post
                  $http
                    .post(API_URL + url, prms, httpOptions)
                    .then(reqSuccess, reqErr);
                } else {
                  // ptException.rejector('API', deferred)(endpoint.http.method + ' method not recognized');
                }
              }

              doCall({ headers: options.headers, timeout: $provider.timeout, retry: 0 });

              return deferred.promise;
            };
          }
        });
      }

      this.$get = ['$q', '$http', '$resource', 'API_URL',
        function ($q, $http, $resource, API_URL) {
        return new API($q, $http, $resource, API_URL);
      }];
    });
}(angular));
